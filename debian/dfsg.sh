#!/bin/sh
format=`echo $3 | sed "s/\(.*\.\)\(.*\)/\2/"`
newname=`echo $3 | sed "s/\(\.orig\)/\+dfsg\1/"`
case $format in
    gz)
        compr=gzip
        ;;
    bz2)
        compr=bzip2
        ;;
    lzma)
        compr=lzma
        ;;
    xz)
        compr=xz
        ;;
esac
$compr -d -c $3 | tar --wildcards --delete `cat debian/files.non-dfsg` | \
$compr -9 -f > $newname
rm -f $3
